#!/usr/bin/env bash

# Basic directories
mkdir -p ~/Documents/{git,playground} ~/go/{src,pkg,bin}

function update_bashrc() {
	echo -e "alias k=kubectl" >> ~/.bashrc
	echo -e "export GOPATH=/home/igor/go" >> ~/.bashrc
	echo -e "export GOBIN=/home/igor/go/bin" >> ~/.bashrc
}

function allow_font_emoji() {
	sudo pacman -S noto-fonts-emoji powerline-fonts --noconfirm
	mkdir -p ~/.config/fontconfig
	echo -e '<!DOCTYPE fontconfig SYSTEM "fonts.dtd"> 
		<fontconfig>
		  <selectfont>
		    <acceptfont>
		      <pattern>
		        <patelt name="family"><string>terminess powerline</string></patelt>
		    </pattern>
		    </acceptfont>
	          </selectfont>
		  <alias>
		    <family>serif</family>
		    <prefer>
		      <family>Noto Color Emoji</family>
		    </prefer>
		  </alias>
		  <alias>
		    <family>sans-serif</family>
		    <prefer>
		      <family>Noto Color Emoji</family>
		    </prefer>
		  </alias>
		  <alias>
		    <family>monospace</family>
		    <prefer>
		      <family>Noto Color Emoji</family>
		    </prefer>
		  </alias>
		</fontconfig>' >> ~/.config/fontconfig/fonts.conf
	fc-cache -f -v
}

# Allow docker to be executed as non-root
function docker_conf() {
	sudo groupadd docker
	sudo usermod -aG docker $USER 
}

function disable_zsh() {
	chsh -s /bin/bash
	sudo pacman -Rcc zsh --noconfirm
}

# update and upgrade system
function upgrade_system() {
	sudo pacman -Syyu --noconfirm
}

# install applications
function install_app() {
	# official applications
	sudo pacman -S terminus-font-otb xclip tree nodejs yarn npm base-devel clang make cmake go yay docker docker-compose kubectl minikube alacritty jdk8-openjdk java8-openjfx vim htop httpie --noconfirm
	# non-official applications
	yay -S google-chrome visual-studio-code-bin --noconfirm
}

function alacritty_file() {
    mkdir -p ~/.config/alacritty/
    cd ~/.config/alacritty/
    curl -O -L "https://gist.githubusercontent.com/barbosaigor/d103b27c703ea56bd018405ad2c5dece/raw/c450ec076ecd32d387a5c9224196d05aa3630433/alacritty.yaml"
    cd ~
}

disable_zsh

upgrade_system

update_bashrc

install_app 

allow_font_emoji

docker_conf

alacritty_file 

